function createCard(name, description, pictureUrl, starts, ends, location) {
  return `
  <div class= "col">
    <div class="card shadow p-3 bg-body-tertiary rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
        <div class="card-footer"> ${starts} - ${ends}</div>
      </div>
    </div>
  </div>
  `;
}

function failedResponseAlert(status) {
  return `
  <div class="alert alert-warning" role="alert">
    There's an error. Status code is ${status}
  </div>
  `
}

function failedTryBlockAlert(error) {
  return `
  <div class="alert alert-danger" role="alert">
  There's an error. The error is ${error}
  </div>
`
}


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);
    const status = response.status

    if (!response.ok) {
      document.body.innerHTML += failedResponseAlert(status)
      console.error("There's an error. Status code is", status)
    } else {
      const data = await response.json();
      // this turns response into something that JS can read
      console.log(data)

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const location = details.conference.location.name
          const start_date = new Date(details.conference.starts);
          // const starts = `${start_date.getMonth()+1}/${start_date.getDate()}/${start_date.getFullYear()}`;
          const starts = start_date.toLocaleDateString();
          const end_date = new Date(details.conference.ends);
          // const ends = `${end_date.getMonth()+1}/${end_date.getDate()}/${end_date.getFullYear()}`;
          const ends = start_date.toLocaleDateString();

          const conference_card = document.querySelector('.conference-card');
          // document refers to DOCTYPE html -
          conference_card.innerHTML += createCard(name, description, pictureUrl, location, starts, ends);
          // we necessarily don't even need to have html
          // we can just invoke the createCard()

          console.log(details);
        }
      }

    }
  } catch (error) {
      document.body.innerHTML += failedTryBlockAlert(error);
      console.error("Error")
    // Figure out what to do if an error is raised
  }

});
